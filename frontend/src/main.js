import Vue from 'vue'
import './components'
import './plugins/vuetify'
import './plugins/VueApexCharts'
import { router } from './routes/index.js';
import { store } from './store/index.js';
import axios from 'axios';
import Vuelidate from 'vuelidate';
import App from './App.vue'

Vue.prototype.$axios = axios;
Vue.config.productionTip = false
Vue.use(Vuelidate);

new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app')
