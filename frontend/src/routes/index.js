import Vue from 'vue';
import VueRouter from 'vue-router';
import ItemView from '../views/ItemView.vue';
import AddInputView from '../views/AddInputView.vue';
import MemberView from '../views/MemberView.vue';
import TeamView from '../views/TeamView.vue';
import WriteView from '../views/WriteView.vue';
import RankView from '../views/RankView.vue';
import ResultView from '../views/ResultView.vue';
// import bus from '../utils/bus.js';
// import { store } from '../store/index.js';

Vue.use(VueRouter);

export const router = new VueRouter({

    mode: 'history',
    routes: [
        {
            path: '/',
            name: '',
            component: AddInputView
        },
        {
            path: '/list',
            name: 'list',
            // component: createListView('NewsView'),
            component: ItemView,
            /*
            beforeEnter: (to, from, next) => { // 인증 정보가 있을 경우 제일 많이 씀.
                // console.log('to', to); // 이동할 위치
                // console.log('from', from); // 현재 위치
                // console.log(next); // Function
                // next(); // 이걸 꼭 해줘야지만 넘어감. 안 쓰면 안 넘어간다.

                bus.$emit('start:spinner');
                // #1
                store.dispatch('FETCH_LIST', to.name)
                    .then(() => {
                        // #5
                        console.log(5);
                        console.log('fetched');
                        bus.$emit('end:spinner');
                        next();
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            },
            */
        },
        {
            path: '/member',
            name: 'member',
            component: MemberView
        },
        {
            path: '/team',
            name: 'team',
            component: TeamView
        },
        {
            path: '/rank',
            name: 'rank',
            component: RankView
        },
        {
            path: '/write',
            name: 'write',
            component: WriteView
        },
        {
            path: '/100',
            name: '100',
            component: ResultView
        },


    ]
})