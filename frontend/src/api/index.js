import axios from 'axios';

async function fetchMatchList(){
    try {
        console.log('api_index => fetchMatchList');
        const response = await axios.get('http://15.164.138.118:3000/match/matchDB');
        console.log(response);
        return response;
    }catch (e) {
        console.log(e);
    }
}
async function fetchCountWin(){
    try {
        console.log('api_index => fetchCountWin');
        const response = await axios.get('http://15.164.138.118:3000/countWin');
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchCreateWin(form){
    try {
        console.log('api_index => fetchCreateWin');
        console.log(form);
        const response = await axios.post('http://15.164.138.118:3000/match/create_process', form);
        console.log(response);
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchRankMatch(){
    try {
        console.log('api_index => fetchRankMatch');
        const response = await axios.get('http://15.164.138.118:3000/match/rankMatch');
        console.log(response);
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchRankPlayer(){
    try {
        console.log('api_index => fetchRankPlayer');
        const response = await axios.get('http://15.164.138.118:3000/match/rankPlayer');
        console.log(response);
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchPlayerMyList() {
    try {
        const response = await  axios.get('http://15.164.138.118:3000/player/myList');
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchPlayerYourList() {
    try {
        const response = await  axios.get('http://15.164.138.118:3000/player/yourList');
        return response;
    }catch (e) {
        console.log(e);
    }
}

async function fetchMemberAllList() {
    try {
        const response = await  axios.get('http://15.164.138.118:3000/player/AllList');
        return response;
    }catch (e) {
        console.log(e);
    }
}


async function fetchTimeLineList() {
    try {
        console.log('api index => TimeLineList');
        const response = await  axios.get('http://15.164.138.118:3000/timeLine/list');
        console.log(response);
        return response;
    }catch (e) {
        console.log(e);
    }
}
async function fetchAddTimeLine(timeLine){
    try{
        console.log('api index => AddTimeLine')
        const response = await axios.post('http://15.164.138.118:3000/timeLine/add', timeLine)
        console.log(response);
        return response;
    }catch (e){
        console.log(e)
    }
}




export {
    fetchMatchList,
    fetchCountWin,
    fetchCreateWin,
    fetchRankMatch,
    fetchPlayerMyList,
    fetchPlayerYourList,
    fetchTimeLineList,
    fetchAddTimeLine,
    fetchMemberAllList,
    fetchRankPlayer,
    
}