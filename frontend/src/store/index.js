import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        match: [],
        myPlayer: {},
        yourPlayer: {},
        all_member: {},
	    single_opponent: {},
	    double_opponent: {},
        timeLineData: [],
        win: 0,
        message: '',
        rank: [],
        rankPlayer: [],
        rankChart: {},
        drawer: null,
        sidebarBackgroundColor: 'rgba(27, 27, 27, 0.74)',
        color: 'success',
    },
    getters,
    mutations,
    actions


});