import {
    fetchMatchList,
    fetchCountWin,
    fetchCreateWin,
    fetchRankMatch,
    fetchPlayerMyList,
    fetchPlayerYourList,
    fetchMemberAllList,
    fetchTimeLineList,
    fetchAddTimeLine,
    fetchRankPlayer,
} from '../api/index.js';

export default {
    async FETCH_MATCH({commit}) {
        try {
            const response = await fetchMatchList();
            commit('SET_MATCH', response.data);
            return response;
        }catch (e) {
            console.log(e);
        }
    },

    async COUNT_WIN({commit}) {
        try {
            const response = await fetchCountWin();
            commit('SET_COUNT_WIN', response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async CREATE_MATCH({commit}, form) {
        try {
            console.log('create_match');
            const response = await fetchCreateWin(form);
            console.log(form);

            commit('SET_CREATE_MATCH', response.data);
            console.log(response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async RANK_MATCH({commit}) {
        try {
            console.log('action,js => RANK_MATCH');
            const response = await fetchRankMatch();

            commit('SET_RANK_MATCH', response.data);
            console.log(response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async RANK_PLAYER({commit}) {
        try {
            console.log('action,js => RANK_PLAYER');
            const response = await fetchRankPlayer();
            commit('SET_RANK_PLAYER', response.data);
            console.log(response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async MY_PLAYER_LIST({commit}) {
        try {
            const response = await fetchPlayerMyList();
            commit('SET_MY_PLAYER_LIST', response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async YOUR_PLAYER_LIST({commit}) {
        try {
            const response = await fetchPlayerYourList();
            commit('SET_YOUR_PLAYER_LIST', response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async All_MEMBER_LIST({commit}) {
        try {
            const response = await fetchMemberAllList();
            commit('SET_All_MEMBER_LIST', response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },
    async TIMELINE_LIST({commit}) {
        try {
            const response = await fetchTimeLineList();
            commit('SET_TIMELINE_LIST', response.data);
            return response;
        } catch (e) {
            console.log(e);
        }
    },

    async TIMELINE_ADD(context, timeline){
        try{
            const response = await fetchAddTimeLine(timeline);
            return response
        }catch (e){
            console.log(e)
        }
    }

}