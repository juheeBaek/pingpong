import { set, toggle } from '../utils/vuex.js';

export default {
    SET_MATCH(state, match_data) {
        console.log(match_data);
        state.match = match_data;
    },
    SET_MY_PLAYER_LIST(state, myPlayerData) {
	    myPlayerData.forEach(function(item) {
	        item['on'] = false;
	        item['color'] = 'grey darken-4';
        });
        state.myPlayer = myPlayerData;
    },
    SET_YOUR_PLAYER_LIST(state, yourPlayerData) {
	    yourPlayerData.forEach(function(item) {
		    item['on'] = false;
		    item['color'] = 'grey darken-2';
	    });
        state.yourPlayer = yourPlayerData;
    },
    SET_All_MEMBER_LIST(state, all_member_data) {
	    all_member_data.forEach(function(item) {
		    item['on'] = false;
            (item.teamId !== 1) ? item['color'] = 'grey darken-2' : item['color'] = 'grey darken-4';
	    });
        state.all_member = all_member_data;
        state.single_opponent = JSON.parse(JSON.stringify( all_member_data ));
        state.double_opponent = JSON.parse(JSON.stringify( all_member_data ));
    },
    SET_COUNT_WIN(state, count_win) {
        state.win = count_win[0]['count_win'];
    },
    SET_CREATE_MATCH(state, message) {
        console.log(message);
        state.message = message;
    },
    SET_RANK_MATCH(state, rank_data) {
        state.rank = rank_data;
    },
    SET_RANK_PLAYER(state, rank_data) {
        state.rankPlayer = rank_data;
    },
    SET_TIMELINE_LIST(state, timeLine_data) {
        state.timeLineData = timeLine_data;
    },
    setDrawer: set('drawer'),
    toggleDrawer: toggle('drawer')
}