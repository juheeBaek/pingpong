/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pingpongTeam', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING(40),
      allowNull: false,
      field: 'name'
    },
    amblem: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'amblem'
    }
  }, {
    tableName: 'pingpong_team',
    timestamps: false,
  });
};
