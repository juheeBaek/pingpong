/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pingpongTimeLine', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date'
    },
    comment: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: 'comment'
    },
    writerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pingpong_player',
        key: 'id'
      },
      field: 'writerId'
    }
  }, {
    tableName: 'pingpong_TimeLine',
    timestamps: false,
  });
};
