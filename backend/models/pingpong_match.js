/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pingpongMatch', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date'
    },
    name: {
      type: DataTypes.STRING(120),
      allowNull: false,
      field: 'name'
    },
    type: {
      type: DataTypes.ENUM('S','D'),
      allowNull: false,
      field: 'type'
    },
    result: {
      type: DataTypes.STRING(20),
      allowNull: false,
      field: 'result'
    },
    myscore: {
      type: DataTypes.INTEGER(30),
      allowNull: true,
      field: 'myscore'
    },
    yourscore: {
      type: DataTypes.INTEGER(30),
      allowNull: true,
      field: 'yourscore'
    }
  }, {
    tableName: 'pingpong_match',
    timestamps: false,
  });
    match.associate = function(models){
      match.hasMany(models.pingpongPlaying)
    }

};
