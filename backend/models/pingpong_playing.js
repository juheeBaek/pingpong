/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pingpongPlaying', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    playerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pingpong_player',
        key: 'id'
      },
      field: 'playerId'
    },
    matchId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pingpong_match',
        key: 'id'
      },
      field: 'matchId'
    },
    teamType: {
      type: DataTypes.ENUM('My','Your'),
      allowNull: false,
      field: 'teamType'
    }
  }, {
    tableName: 'pingpong_playing',
    timestamps: false,
  });
};
