/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pingpongPlayer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      field: 'name'
    },
    grip: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: 'grip'
    },
    avatar: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'avatar'
    },
    one: {
      type: DataTypes.STRING(150),
      allowNull: true,
      field: 'one'
    },
    position: {
      type: DataTypes.STRING(55),
      allowNull: true,
      field: 'position'
    },
    teamId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pingpong_team',
        key: 'id'
      },
      field: 'teamId'
    },
    nick: {
      type: DataTypes.STRING(30),
      allowNull: true,
      field: 'nick'
    }
  }, {
    tableName: 'pingpong_player',
    timestamps: false,
  });
};
