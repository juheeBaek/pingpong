'use strict';

const path = require('path');
const Sequelize = require('sequelize');

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, config);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.Match = require('./pingpong_match')(sequelize, Sequelize)
db.Player = require('./pingpong_player')(sequelize, Sequelize)
db.Playing = require('./pingpong_playing')(sequelize, Sequelize)
db.Team = require('./pingpong_team')(sequelize, Sequelize)
db.TimeLine = require('./pingpong_TimeLine')(sequelize, Sequelize)

db.Match.hasMany(db.Playing, {foreignKey:'matchId', sourceKey: 'id'});
db.Player.hasMany(db.Playing, {foreignKey:'playerId', sourceKey: 'id'});

db.Playing.belongsTo(db.Match, {foreignKey:'matchId', targetKey: 'id'})
db.Playing.belongsTo(db.Player, {foreignKey:'playerId', targetKey: 'id'})


module.exports = db;

