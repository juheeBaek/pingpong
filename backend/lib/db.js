var mysql = require('mysql');
var db_config = require('../config/config.db.json');

var db = mysql.createConnection({
    host     : db_config.host,
    user     : db_config.user,
    password : db_config.password,
    database : db_config.database,
});
db.connect();

// 1개일 때
module.exports = db;
