var express = require('express');
var router = express.Router();
var db = require('../lib/db.js');
var TimeLine = require('../models').TimeLine;
var sequelize = require('../models').sequelize;

/* GET users listing. */
router.get('/', function(req, res) {
        res.send('timeLine');
});

router.get('/list', (req, res, next) => {
    TimeLine.findAll({
        attributes:[
            'id',
            [sequelize.fn('date_format', sequelize.col('date'), '%m/%d %H:%i'), 'date'],
            'comment', 'writerId'
        ],
        order:[
            ['date', 'DESC']
        ],
    })
        .then((timeline) => {
            res.send(timeline);
        })
        .catch((error) => {
            res.send(error);
        })
});

router.post('/add', (req, res, next) =>{
    TimeLine.create({
        date: req.body.date,
        oneText: req.body.comment,
        writerName: "SL",
        writerId: '2',
    })
        .then((data) => {
            res.send(data);
        })
        .catch((error) => {
            res.send(error);
        })
});



module.exports = router