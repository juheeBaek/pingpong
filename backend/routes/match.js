var express = require('express');
var router = express.Router();
var db = require('../lib/db.js');
var path = require('path');
var Match = require('../models').Match;
var Player = require('../models').Player;
var Playing = require('../models').Playing;
var sequelize = require('../models').sequelize;

/* GET users listing. */
router.get('/', function(req, res) {
    console.log('match');

    Match.findAll({
        include:[
            {
                model: Playing,
            }
        ],
        order: [
            ['id', 'DESC'],
        ],
    }).then(data => {
        console.log(data)
        res.send(data)
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
    res.send('respond with a resource');
});

router.get('/create_process', (req, res) => {
    // res.send('respond create_match');
    console.log(req);
    console.log(res);
    res.sendFile(path.match(__dirname, '/match/create_process'));
});

router.post('/create_process', (req, res) => {
    console.log('post_create_match_process');
    const data = req.body;
    console.log(data);
    console.log('================');
    let t;
    let match_id
    sequelize.transaction(transaction =>{
        console.log("TranSaction Start CREATE MATCH ROW");
        t = transaction;
        return Match.create({
            date: data.date,
            name: data.name,
            type: data.type,
            result: data.result,
        }, {transaction: t}).then(function (result){
            match_id = result.dataValues.id
            if(data.type === 'S') {
                console.log('Single');
                return Playing.create({
                    playerId: data.my1,
                    matchId: match_id,
                    teamType: 1,
                }, {transaction: t}).then(result => {
                    return Playing.create({
                        playerId: data.your1,
                        matchId: match_id,
                        teamType: 2,
                    }, {transaction: t})
                })
            }else {
                console.log('Double');
                return Playing.create({
                    playerId: data.my1,
                    matchId: match_id,
                    teamType: 1,
                }, {transaction: t}).then(result => {
                    return Playing.create({
                        playerId: data.my2,
                        matchId: match_id,
                        teamType: 1,
                    }, {transaction: t}).then(result => {
                        return Playing.create({
                            playerId: data.your1,
                            matchId: match_id,
                            teamType: 2,
                        }, {transaction: t}).then(result => {
                            return Playing.create({
                                playerId: data.your2,
                                matchId: match_id,
                                teamType: 2,
                            }, {transaction: t})
                        })
                    })
                })
            }
        }).then(function (result){
            console.log('transaction success', result);
            t.commit();
            res.send(result)
        }).catch(function (err){
            t.rollback();
            console.log('transaction fail ', err);
            res.send(err)
        })
    });
});

router.get('/matchDB', (req, res, next) => {

    Match.findAll({
        attributes:[
            'id', 'name', 'type', 'result',
            [sequelize.fn('date_format', sequelize.col('date'), '%Y-%m-%d'), 'date']
        ],
        order:[
            ['date', 'DESC']
        ],
        include:[
            {
                model: Playing,
                include:[
                    {
                        model: Player,
                    }
                ]
            }
        ],
    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
});

router.get('/rankMatch', (req, res) => {
    console.log('get_rankMatch_Start');

    Player.findAll({
        attributes:[

            'id', 'name', 'teamId'
        ],
        include:[
            {
                model: Playing,
                attributes: [
                    [sequelize.fn('COUNT', sequelize.col('pingpongPlayings.matchId')), 'PlayingCount']
                ]
            }
        ],
        group:[
          'name',
        ],
        where:{
            teamId: 1
        },
    }).then(result =>{
        console.log(result)
        res.send(result)
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
});

router.get('/rankPlayer', (req, res) => {
    console.log('get_rankPlayer_Start');

    Match.findAll({
        attributes:[
            'id',
        ],
        include:[
            {
                model: Playing,
                attributes: [
                    [Player.name, 'MyTeam']
                ],
                include:[
                    {
                        model: Player,
                        where : {
                            teamType: 'My'
                        },
                        group:[
                            'matchID',
                        ],
                    }
                ],

            },
         /*   {
                model: [Playing, Player],
                attributes: [
                    [Player.name, 'YourTeam']
                ],
                group:[
                    'matchID',
                ],
                where : {
                    playerId: Player.id,
                    matchId : Match.id,
                    teamType: 'Your'
                },
            },
            {
                model: Match,
                attributes: [
                    [sequelize.fn('COUNT', sequelize.col('pingpongMatchs.id')), 'win_count']
                ]
            }*/
        ],
        group:[
          'MyTeam'
        ],
        where:{
            type: 'S'
        },
    }).then(result =>{
        console.log(result)
        res.send(result)
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
});

module.exports = router