var express = require('express');
var router = express.Router();
var db = require('../lib/db.js');
var Player = require('../models').Player;


/* GET users listing. */
router.get('/', function(req, res) {
        console.log('player');
        res.send('respond with a resource');
});

router.get('/myList', (req, res, next) => {
    Player.findAll({
        where: {
            teamId : 1,
        }
    })
        .then((players) => {
            res.send(players);
        })
        .catch((error) => {
            res.send(error);
        })
});

router.get('/yourList', (req, res, next) => {
    Player.findAll({
        where: {
            teamId : 99,
        }
    })
        .then((players) => {
            res.send(players);
        })
        .catch((error) => {
            res.send(error);
        })
});

router.get('/AllList', (req, res, next) => {
    Player.findAll({})
        .then((players) => {
            console.log("success")
            res.send(players);
        })
        .catch((error) => {
            console.log(error)
            res.send(error);
        })
});


module.exports = router