var express = require('express');
var router = express.Router();
var db = require('../lib/db.js');
var match = require('./match.js');
var player = require('./player.js');
var timeLine = require('./timeLine.js');
const createError = require('http-errors');
var sequelize = require('../models').sequelize;

router.use('/match', match);
router.use('/player', player);
router.use('/timeLine', timeLine);
var Match = require('../models').Match;

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', { title: 'Express' });
});

router.get('/countWin', (req, res) => {
    Match.findAll({
       attributes:[
            [sequelize.fn('COUNT', sequelize.col('id')), 'count_win']
       ],
       where: {
           result: 'W'
       }
    }).then(count => res.send(count))
      .catch(err => res.send(err));
});

module.exports = router;
